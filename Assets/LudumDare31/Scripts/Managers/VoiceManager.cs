﻿using UnityEngine;
using System.Collections;

public class VoiceManager : MonoBehaviour {

	public AudioClip		FightClip;
	public AudioClip		AwesomeClip;
	public AudioClip		FinishClip;
	public AudioClip		HeadshotClip;
	public AudioClip		HumanClip;
	public AudioClip		LoserClip;
	public AudioClip		NinjaClip;
	public AudioClip		NutcrackerClip;
	public AudioClip		SnowmanClip;
	public AudioClip		SuperbClip;
	public AudioClip		VersusClip;
	public AudioClip		WinnerClip;
	public AudioClip		RammingClip;
	public AudioClip		PlayerOneClip;
	public AudioClip		PlayerTwoClip;
	public AudioClip		DummyClip;
	public AudioClip		GiantClip;
	public AudioClip		TrollClip;
	public AudioClip		SoccerClip;
	public AudioClip		ChuckClip;
	
	public void PlayCharacter(string name) {
	
		if (name.ToLower() == "human")
			PlayHuman();
			
		if (name.ToLower() == "ninja")
			PlayNinja();
		
		if (name.ToLower() == "snowman")
			PlaySnowman();
		
		if (name.ToLower() == "nutcracker")
			PlayNutcracker();
		
		if (name.ToLower() == "dummy")
			PlayDummy();
		
		if (name.ToLower() == "giant")
			PlayGiant();
		
		if (name.ToLower() == "troll")
			PlayTroll();
		
		if (name.ToLower() == "soccer")
			PlaySoccer();
		
		if (name.ToLower() == "chuck")
			PlayChuck();
		
	}
	
	
	public void PlayChuck() {
		playSound(ChuckClip);
	}
	
	public void PlayGiant() {
		playSound(GiantClip);
	}
	
	public void PlayTroll() {
		playSound(TrollClip);
	}
	
	public void PlaySoccer() {
		playSound(SoccerClip);
	}
	
	public void PlayDummy() {
		playSound(DummyClip);
	}
	
	public void PlayPlayerOne() {
		playSound(PlayerOneClip);
	}
	
	public void PlayPlayerTwo() {
		playSound(PlayerTwoClip);
	}
	
	public void PlayRamming() {
		playSound(RammingClip);
	}
	
	public void PlayFight() {
		playSound(FightClip);
	}

	public void PlayAwesome() {
		playSound(AwesomeClip);
	}
	
	public void PlayFinish() {
		playSound(FinishClip);
	}
	
	public void PlayHeadshot() {
		playSound(HeadshotClip);
	}
	
	public void PlayHuman() {
		playSound(HumanClip);
	}
	
	public void PlayLoser() {
		playSound(LoserClip);
	}
	
	public void PlayNinja() {
		playSound(NinjaClip);
	}
	
	public void PlayNutcracker() {
		playSound(NutcrackerClip);
	}
	
	public void PlaySnowman() {
		playSound(SnowmanClip);
	}
	
	public void PlaySuperb() {
		playSound(SuperbClip);
	}
	
	public void PlayVersus() {
		playSound(VersusClip);
	}
	
	public void PlayWinner() {
		playSound(WinnerClip);
	}
	
	private void playSound(AudioClip clip) {
		
		D.Trace("[VoiceManager] playSound");
		playSound(clip, 1.0f, 0.5f);
	}
	
	private void playSound(AudioClip clip, float pitch, float volume) {
		
		D.Trace("[VoiceManager] playSound");
		
		GameObject go = new GameObject("Voice (Temp)");
		AudioSource gs = go.AddComponent<AudioSource>();
		gs.clip = clip;
		gs.volume = volume;
		gs.pitch = pitch;
		gs.Play();
		Destroy(go, clip.length + 0.1f);
	}
	
	//	MONO
	
	void OnEnable() {
		D.Trace("[SoundManager] OnEnable");
		GameController.Instance.VoiceManager = this;
	}
}
