﻿using UnityEngine;
using System.Collections;

public class WallpaperManager : MonoBehaviour {

	public GameObject[]		Wallpapers;
	
	public void ShowWallpaper() {
		hideAll();	
		int i = Random.Range(0,Wallpapers.Length);
		Wallpapers[i].transform.localPosition = new Vector2(0,3.6f);
	}
	
	private void hideAll() {
		foreach(GameObject go in Wallpapers) {
			go.transform.localPosition = new Vector2(0,-10000);
		}
	}
	
	void OnEnable() {
		GameController.Instance.WallpaperManager = this;
	}
}
