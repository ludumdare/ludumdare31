﻿using UnityEngine;
using System.Collections;

public class SkinManager : MonoBehaviour {

	public tk2dSpriteCollectionData			SkinDummy;
	public tk2dSpriteCollectionData			SkinSnowman;
	public tk2dSpriteCollectionData			SkinHuman;
	public tk2dSpriteCollectionData			SkinNinja;
	public tk2dSpriteCollectionData			SkinTroll;
	public tk2dSpriteCollectionData			SkinSoccer;
	public tk2dSpriteCollectionData			SkinGiant;
	public tk2dSpriteCollectionData			SkinNerd;
	public tk2dSpriteCollectionData			SkinSanta;
	public tk2dSpriteCollectionData			SkinNutcracker;
	public tk2dSpriteCollectionData			SkinChuck;
	
	public tk2dSpriteCollectionData GetSkin(string name) {
		D.Trace("[SkinManager] GetSkin");
		D.Log("looking for {0} skin", name);
		
		D.Trace("[SkinManager] GetSkin");
		
		if (name.ToLower() == "snowman")
			return SkinSnowman;
	
		if (name.ToLower() == "human")
			return SkinHuman;
		
		if (name.ToLower() == "ninja")
			return SkinNinja;
		
		if (name.ToLower() == "troll")
			return SkinTroll;
		
		if (name.ToLower() == "soccer")
			return SkinSoccer;
		
		if (name.ToLower() == "giant")
			return SkinGiant;
		
		if (name.ToLower() == "nerd")
			return SkinNerd;
		
		if (name.ToLower() == "santa")
			return SkinSanta;
		
		if (name.ToLower() == "chuck")
			return SkinChuck;
		
		if (name.ToLower() == "nutcracker")
			return SkinNutcracker;
		
		//	the default if nothing else was found
		return SkinDummy;		
	}
	
	void OnEnable() {
		GameController.Instance.SkinManager = this;
	}
}
