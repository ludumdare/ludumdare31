﻿using UnityEngine;
using System.Collections;

public class PlayerOne : MonoBehaviour {

	public UnityEngine.UI.Text		PlayerScore;
	public UnityEngine.UI.Image		PlayerHealth;
	public UnityEngine.UI.Image		PlayerPower;
	public UnityEngine.UI.Text		PlayerHealthText;
	public UnityEngine.UI.Text		PlayerPowerText;
	
	private PlayerStats		playerStats;
	private float 			sizeHealth = 300;
	private float 			sizePower = 300;
	private float			startHealth;
	
	public void StartGame() {
		playerStats = GameController.Instance.ScoreManager.PlayerOne;
		playerStats.Score = 0;
		playerStats.Coins = 0;
		playerStats.Experience = 0;
		playerStats.Power = 0;
		startHealth = playerStats.Health;
		StartCoroutine(updateGui());
	}
	
	public void StopGame() {
		StopCoroutine(updateGui());
	}
	
	private IEnumerator updateGui() {
		while(true) {
			yield return new WaitForSeconds(0.5f);
			PlayerScore.text = playerStats.Score.ToString();	
			PlayerHealthText.text = playerStats.Health.ToString();
			PlayerPowerText.text = playerStats.Power.ToString();
			
			float hs = sizeHealth * (playerStats.Health/startHealth);
			//D.Warn(hs);
			PlayerHealth.rectTransform.sizeDelta = new Vector2(hs, PlayerHealth.rectTransform.sizeDelta.y);
		}
	}
}
