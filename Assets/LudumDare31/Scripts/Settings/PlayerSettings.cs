﻿using UnityEngine;
using System.Collections;

public class PlayerSettings : MonoBehaviour {

	public string		PlayerName			{ get; set; }
	public string		PlayerCharacter		{ get; set; }
	public string		PlayerHiscore		{ get; set; }
	public string		PlayerLevel			{ get; set; }
	public string		PlayerExperience	{ get; set; }
	public string		PlayerStrength		{ get; set; }
	public string		PlayerStamina		{ get; set; }
	public string		PlayerSpeed			{ get; set; }
	public string		PlayerLeftHand		{ get; set; }
	public string		PlayerRightHand		{ get; set; }
	
	void OnEnable() {
		GameController.Instance.PlayerSettings = this;
	}
	
}
