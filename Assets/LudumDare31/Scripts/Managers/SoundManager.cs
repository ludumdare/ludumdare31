﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour {

	public AudioClip		SoundJump;
	public AudioClip		SoundHit;
	
	//	PUBLIC
	
	public void PlayJump() {
		__playSound(SoundJump);
	}
	
	public void PlayHit() {
		playPitchSound(SoundHit);
	}
	
	//	PRIVATE
	
	private void playPitchSound(AudioClip clip) {
		__playSound(clip, Random.Range(0.2f,0.8f), 1.0f);
	}
	
	private void __playSound(AudioClip clip) {
		
		D.Trace("[GameSounds] __playSound");
		__playSound(clip, 1.0f, 0.5f);
	}
	
	private void __playSound(AudioClip clip, float pitch, float volume) {
		
		D.Trace("[GameSounds] __playSound");
		
		GameObject go = new GameObject("Sound (Temp)");
		AudioSource gs = go.AddComponent<AudioSource>();
		gs.clip = clip;
		gs.volume = volume;
		gs.pitch = pitch;
		gs.Play();
		Destroy(go, clip.length + 0.1f);
	}
	
	private GameObject __playSoundLoop(AudioClip clip) {
		
		D.Trace("[GameSounds] __playSound");
		//audio.pitch = pitch;
		//audio.PlayOneShot(clip, volume);
		//	reset
		//audio.pitch = 1.0f;
		
		GameObject go = new GameObject("Sound (Temp)");
		AudioSource gs = go.AddComponent<AudioSource>();
		gs.clip = clip;
		gs.loop = true;
		gs.Play();
		return go;
	}
	
	//	MONO
	
	void OnEnable() {
		D.Trace("[SoundManager] OnEnable");
		GameController.Instance.SoundManager = this;
	}
}
