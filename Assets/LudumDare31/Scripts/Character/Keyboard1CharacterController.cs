﻿using UnityEngine;
using System.Collections;

public class Keyboard1CharacterController : CharacterController {

	void Update () {
		
		if (Input.GetKeyDown(KeyCode.D)) {
			AttackRight();
		}	
		
		if (Input.GetKeyDown(KeyCode.A)) {
			AttackLeft();
		}	
		
		if (Input.GetKeyDown(KeyCode.W)) {
			Jump();
		}	
		
	}
}
