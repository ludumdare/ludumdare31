﻿using UnityEngine;
using System.Collections;

public class CharacterBody : MonoBehaviour {

	public CharacterController		controller	{ get; set; }

	void OnTriggerEnter2D(Collider2D other) {
		D.Fine("[CharacterController] OnTriggerEnter");
		D.Log("collided with {0}", other.tag);
		if (other.tag == "Floor")
			SendMessageUpwards("Idle", SendMessageOptions.DontRequireReceiver);
	}
	
	void OnTriggerExit2D(Collider2D other) {
		D.Fine("[CharacterController] OnTriggerEnter");
		D.Log("collided with {0}", other.tag);
		//if (other.tag == "Floor")
		//	SendMessageUpwards("Airborn", SendMessageOptions.DontRequireReceiver);
	}
	
	void Start() {
		D.Log(controller);
		if (controller != null)
			controller.Idle();
	}
	
}
