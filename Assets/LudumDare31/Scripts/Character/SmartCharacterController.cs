﻿using UnityEngine;
using System.Collections;

public class SmartCharacterController : CharacterController {

	//	PUBLIC
	
	public override void OnStart ()
	{
		base.OnStart ();
		StartCoroutine(randomThings());
		
	}
	
	//	PRIVATE
	
	private IEnumerator randomThings() {
		while(true) {
			
			yield return new WaitForSeconds(0.15f);
			
			int r = Random.Range(0,100);
			D.Log(r);
			
			if (r < 60) {
				Idle();
				continue;
			}
			
			if (r < 70) {
				Jump();
				continue;
			}
			
			if (r < 85) {
				AttackLeft();
				continue;
			}
			
			if (r < 100) {
				AttackRight();
				continue;
			}
		}
	}
	
	//	MONO
}
