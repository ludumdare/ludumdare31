﻿using UnityEngine;
using System.Collections;

public class GameSettings : MonoBehaviour {

	public string		GameName		{ get; set; }
	public string		GameVersion		{ get; set; }
	public bool			GameBlood		{ get; set; }
	
	void Start() {
		
	}
	
	void OnEnable() {
		GameController.Instance.GameSettings = this;
		
		GameName = "Slam Dunkin' Beat the Crap Our Of Everybody";
		GameVersion = "1.0";
		GameBlood = true;
	}
}
