﻿using UnityEngine;
using System.Collections;

public class GameController : MonoSingleton<GameController> {

	public GameManager			GameManager			{ get; set; }
	public GameSettings			GameSettings		{ get; set; }
	public PlayerSettings		PlayerSettings		{ get; set; }
	public MusicManager			MusicManager		{ get; set; }
	public SoundManager			SoundManager		{ get; set; }	
	public CharacterManager		CharacterManager	{ get; set; }
	public SkinManager			SkinManager			{ get; set; }
	public ScoreManager			ScoreManager		{ get; set; }
	public PlayerManager		PlayerManager		{ get; set; }
	public EffectManager		EffectManager		{ get; set; }
	public VoiceManager			VoiceManager		{ get; set; }
	public GuiManager			GuiManager			{ get; set; }
	public WallpaperManager		WallpaperManager	{ get; set; }

	//	PUBLIC
	
	//	PRIVATE
	
	//	MONO
	
	void Start() {
		MusicManager.PlayTitleMusic();
		WallpaperManager.ShowWallpaper();
	}
	
}
