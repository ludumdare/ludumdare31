﻿using UnityEngine;
using System.Collections;

public enum CharacterStates {
	
	IDLE,
	ATTACK_LEFT,
	ATTACK_RIGHT,
	ATTACKING,
	JUMP,
	IN_AIR,
	SPECIAL,
	KNOCKOUT,
	MOVING,
	TIRED
}

public enum EnemyFacing {
	
	LEFT,
	RIGHT
}

public class Character : MonoBehaviour {

	public string		Name;		//	name of the character
	public int			Strength;	//	strength of the character
	public int			Stamina;	//	stamina of the character
	public int			Aggression;	//	how aggressive is the character
	public int			Recovery;	//	rate at which character recovers
	public int			Defense;	//	how defensive is the character
	public string		Prefab;		//	name of the prefab to use
	public string		Skin;		//	name of the skin to use
	public int			Size;		//	size of the character (1-4)
	public int			Speed;		//	speed of the character
	public string		HoldLeft;	//	what is in the left hand
	public string		HoldRight;	//	what is in the right hand
	public string 		Controller;	//	name of the controller to use
	public string		Player;		//	which player 1-4
	public string		Enemy;		//	the enemy we are facing
	public string		Hat;		//	what hat is he wearing
	public int			Health;		//	starting health
	
	private tk2dSpriteCollectionData	skinData;
	
	private tk2dSprite					head;
	private tk2dSprite					body;
	private tk2dSprite					arm1;
	private tk2dSprite					arm2;
	private tk2dSprite					leg1;
	private tk2dSprite					leg2;
	private tk2dSprite					hand1;
	private tk2dSprite					hand2;
	private tk2dSprite					hat;
	private tk2dSprite					hold1;
	private tk2dSprite					hold2;
	
	private Transform					enemyTransform;
	
	private CharacterController			controller;
	
	public CharacterController GetController() {
		return controller;
	}
	
	public void setHolding() {
	
	}
	
	public void setHat() {
	
		if (!string.IsNullOrEmpty(Hat)) {
			hat.SetSprite(Hat);
			hat.gameObject.SetActive(true);
		} else {
			hat.gameObject.SetActive(false);
		}
	}
	
	private void setController() {
		if (string.IsNullOrEmpty(Controller)) {
			controller = transform.gameObject.AddComponent<CharacterController>();
		}
		
		if (Controller == "DisplayCharacterController") {
			controller = transform.gameObject.AddComponent<DisplayCharacterController>();
		}
		
		if (Controller == "DumbCharacterController") {
			controller = transform.gameObject.AddComponent<DumbCharacterController>();
		}
		
		if (Controller == "DumberCharacterController") {
			controller = transform.gameObject.AddComponent<DumberCharacterController>();
		}
		
		if (Controller == "DumbestCharacterController") {
			controller = transform.gameObject.AddComponent<DumbestCharacterController>();
		}
		
		if (Controller == "Keyboard1CharacterController") {
			controller = transform.gameObject.AddComponent<Keyboard1CharacterController>();
		}
		
		if (Controller == "Keyboard2CharacterController") {
			controller = transform.gameObject.AddComponent<Keyboard2CharacterController>();
		}
		
		if (Controller == "DummyCharacterController") {
			controller = transform.gameObject.AddComponent<DummyCharacterController>();
		}
		
		if (Controller == "SmartCharacterController") {
			controller = transform.gameObject.AddComponent<SmartCharacterController>();
		}
		
	}
	
	private void setSkin() {
		D.Trace("[Character] setSkin");
		skinData = GameController.Instance.SkinManager.GetSkin(Skin);
		
		head.Collection = skinData;
		body.Collection = skinData;
		arm1.Collection = skinData;
		arm2.Collection = skinData;
		leg1.Collection = skinData;
		leg2.Collection = skinData;
		hand1.Collection = skinData;
		hand2.Collection = skinData;
		
		head.SetSprite("head");		head.Build();
		body.SetSprite("body");		body.Build();
		arm1.SetSprite("arm1");		arm1.Build();
		arm2.SetSprite("arm2");		arm2.Build();
		leg1.SetSprite("leg1");		
		leg2.SetSprite("leg2");
		hand1.SetSprite("hand1");
		hand2.SetSprite("hand2");
		
		hold1.SetSprite(Random.Range(0,95));
		hold2.SetSprite(Random.Range(0,95));
	}
	
	void setPlayer() {
		D.Trace("[Character] setPlayer");
		changeTagRecursively(transform, Player);	
		changeLayerRecursively(transform, Player);
	
	}
	
	private void changeTagRecursively(Transform trans, string tag)	{
		D.Trace("[Character] changeTagRecursively");
		trans.gameObject.tag = tag;
		if(trans.GetChildCount() > 0)
			foreach(Transform t in trans)
				changeTagRecursively(t, tag);
	}
	
	private void changeLayerRecursively(Transform trans, string layer) {
		D.Trace("[Character] changeLayerRecursively");
		foreach (Transform child in trans) {
			child.gameObject.gameObject.layer = LayerMask.NameToLayer(layer);
			changeLayerRecursively(child, layer);
		}
	}
	
	private void setFacing(EnemyFacing facing) {
		D.Trace("[Character] setFacing");
		bool flip = false;
		
		if (facing == EnemyFacing.RIGHT)
			flip = true;
			
		head.FlipX = flip;		head.Build();
		body.FlipX = flip;		body.Build();
		arm1.FlipX = flip;		arm1.Build();
		arm2.FlipX = flip;		arm2.Build();
		leg1.FlipX = flip;		leg1.Build();
		leg2.FlipX = flip;		leg2.Build();
		hand1.FlipX = flip;		hand1.Build();
		hand2.FlipX = flip;		hand2.Build();
	}
	
	private IEnumerator updateFacing() {
		D.Trace("[Character] updateFacing");
		EnemyFacing last;
		last = findEnemyFacing();
		while(true) {
			EnemyFacing facing = findEnemyFacing();	
			D.Detail("{0} is to the {1} of {2}", Enemy, facing, Player);
			
			if (facing != last) {
				setFacing(facing);			
			}
			
			last = facing;
			
			yield return new WaitForSeconds(1.0f);
		}
	}
	
	private EnemyFacing findEnemyFacing() {
		D.Trace("[Character] findEnemyFacing");
		Vector3 rp = transform.InverseTransformPoint(enemyTransform.position);
		if (rp.x < 0)
			return EnemyFacing.LEFT;
		if (rp.x >= 0)
			return EnemyFacing.RIGHT;
			
		return EnemyFacing.RIGHT;
	}
	
	

	// Use this for initialization
	public void StartGame () {
		D.Log("[Character] StartGame");
		
		hand1.transform.GetComponent<CharacterHand>().StartGame();
		hand2.transform.GetComponent<CharacterHand>().StartGame();
		hold1.transform.GetComponent<CharacterWeapon>().StartGame();
		hold2.transform.GetComponent<CharacterWeapon>().StartGame();
		leg1.transform.GetComponent<CharacterLeg>().StartGame();
		leg2.transform.GetComponent<CharacterLeg>().StartGame();
		head.transform.GetComponent<CharacterHead>().StartGame();
		setController();
		controller.StartGame();
		enemyTransform = GameObject.Find(Enemy).transform.FindChild("Body");		
		StartCoroutine(updateFacing());
	}
	
	public void Init() {
		D.Log("[Character] Init");
		//transform.localScale = new Vector3(4+Size, 4+Size,1);
		gameObject.name = Player;
		setSkin();	
		setPlayer();
		setHolding();
		setHat();
	}
	
	void OnEnable() {
		
		//	get the body parts
		
		head = transform.FindChild("Head").GetComponent<tk2dSprite>();
		body = transform.FindChild("Body").GetComponent<tk2dSprite>();
		arm1 = transform.FindChild("Arm1").GetComponent<tk2dSprite>();
		arm2 = transform.FindChild("Arm2").GetComponent<tk2dSprite>();
		leg1 = transform.FindChild("Leg1").GetComponent<tk2dSprite>();
		leg2 = transform.FindChild("Leg2").GetComponent<tk2dSprite>();
		hand1 = transform.FindChild("Arm1/Hand1").GetComponent<tk2dSprite>();
		hand2 = transform.FindChild("Arm2/Hand2").GetComponent<tk2dSprite>();
		hat = transform.FindChild("Hat").GetComponent<tk2dSprite>();
		hold1 = transform.FindChild("Arm1/Hand1/Hold1").GetComponent<tk2dSprite>();
		hold2 = transform.FindChild("Arm2/Hand2/Hold2").GetComponent<tk2dSprite>();
		
	}
	
	void Update() {
	}
	
}
