﻿using UnityEngine;
using System.Collections;

public class CharacterController : MonoBehaviour {

	public CharacterStates		CurrentState;
	public CharacterStates		LastState;

	private Character			character;	//	every character will have this component
	private CharacterStates		state;		//	current state of the character
	private CharacterStates		lastState;
	private Rigidbody2D			body;		//	the body object rigibody
	private Rigidbody2D			arm1;		//	arm1
	private Rigidbody2D			arm2;		//	arm2
	
	private float				startY;		//	starting y position
	
	private bool				active;		//	
	private bool				isAttacking;
	

	//	PUBLIC
	
	public Character GetCharacter() {
		return character;
	}
	
	public CharacterStates GetState() {
		return state;
	}
	
	public void Jump() {
		D.Trace("[CharacterController] Jump");
		if (state != CharacterStates.IN_AIR)
			state = CharacterStates.JUMP;
	}
	
	public void AttackLeft() {
		D.Trace("[CharacterController] AttackLeft");
		state = CharacterStates.ATTACK_LEFT;
	}
	
	public void AttackRight() {
		D.Trace("[CharacterController] AttackRight");
		state = CharacterStates.ATTACK_RIGHT;
	}
	
	public void Airborn() {
		state = CharacterStates.IN_AIR;
	}
	
	public void Special() {
		D.Trace("[CharacterController] Special");
		state = CharacterStates.SPECIAL;
	}
	
	public void Knockout() {
		D.Trace("[CharacterController] Special");
		state = CharacterStates.KNOCKOUT;
	}
	
	public void Idle() {
		D.Trace("[CharacterController] Idle");
		state = CharacterStates.IDLE;
	}
	
	public virtual void OnFixedUpdate() {
	
		D.Fine("[CharacterController] OnFixedUpdate");
		
		if (state == CharacterStates.IDLE) {
			isAttacking = false;
			return;
		}
		
		if (state == CharacterStates.JUMP) {
			state = CharacterStates.ATTACKING;
			isAttacking = true;
			arm1.AddForce(new Vector2(0,(character.Strength+character.Speed)/2), ForceMode2D.Impulse);
			arm2.AddForce(new Vector2(0,(character.Strength+character.Speed)/2), ForceMode2D.Impulse);
			return;
		}
		
		if (state == CharacterStates.ATTACK_LEFT) {
			if (isAttacking) state = CharacterStates.ATTACKING;
			if (!isAttacking) state = CharacterStates.MOVING;
			body.AddForce(new Vector2(-character.Speed*2,character.Strength), ForceMode2D.Impulse);
			GameController.Instance.SoundManager.PlayJump();
			return;
		}
		
		if (state == CharacterStates.ATTACK_RIGHT) {
			if (isAttacking) state = CharacterStates.ATTACKING;
			if (!isAttacking) state = CharacterStates.MOVING;
			body.AddForce(new Vector2(character.Speed*2,character.Strength), ForceMode2D.Impulse);
			GameController.Instance.SoundManager.PlayJump();
			return;
		}
		
	}
	
	public virtual void OnStart() {
		D.Trace("[CharacterController] OnStart");
	}
	
	public virtual void StartGame() {
		active = true;
		startY = body.transform.position.y;
		StartCoroutine(resetRotation());
	}
	
	public virtual void EndGame() {
		active = false;
		StopCoroutine("resetRotation");
	}
		
	//	PRIVATE
	
	private void updatePosition() {
		D.Fine("[CharacterController] updatePosition");
		if (body.transform.position.y < startY) {
			body.MovePosition(new Vector2 (body.transform.position.x, Mathf.Lerp(body.transform.position.y,startY,Time.deltaTime*character.Recovery)));
			body.MoveRotation(Mathf.Lerp(body.rotation,0,Time.deltaTime*character.Recovery));
		}
	}
	
	private IEnumerator resetRotation() {
		while(true) {
			yield return new WaitForSeconds(1.0f);
			body.MoveRotation(0);
		}
	}
	
	//	MONO
	
	void FixedUpdate() {
		
		if (!active)
			return;
			
		D.Fine("[CharacterController] FixedUpdate");
		updatePosition();
		lastState = state;
		LastState = state;
		OnFixedUpdate();
		CurrentState = state;
	}
	
	void OnEnable() {
		D.Trace("[CharacterController] OnEnable");
		character = GetComponent<Character>();
		body = transform.FindChild("Body").GetComponent<Rigidbody2D>();
		//body.transform.GetComponent<CharacterBody>().controller = this;
		arm1 = transform.FindChild("Arm1").GetComponent<Rigidbody2D>();
		arm2 = transform.FindChild("Arm2").GetComponent<Rigidbody2D>();
	}
	
	// Use this for initialization
	void Start () {
		D.Trace("[CharacterController] Start");
		OnStart();
	}
	
	// Update is called once per frame
	void Update () {
		D.Fine("[CharacterController] Update");
	}
}
